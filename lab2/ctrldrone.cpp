#define S_FUNCTION_NAME ctrldrone
#include "ttkernel.cpp"
#include "mex.h"

#include <math.h>
#include <time.h>
#include <cmath>
#include <stdlib.h>

#define PI 3.141592

//Analog inputs
#define POSX 1
#define POSY 2
#define POSZ 3
#define ORIENTATION 4
#define HSPEED 5
#define VSPEED 6
#define BATT_LEVEL 7
#define IS_PIC_TAKEN 8

//Trigger input
#define PIC_SENT 1
#define ALARM_BAT_FULL 2
#define ALARM_BAT_10 3

//outputs
#define CONS_OR 1
#define CONS_HSPEED 2
#define CONS_VSPEED 3
#define TAKE_PIC 4
#define SEND_PIC 5
#define CHARGE_BAT 6

// Period
#define NAV_CTRL_PERIOD 0.1 //10 Hz frequency
#define CAM_CTRL_PERIOD 0.1 //10 Hz frequency

//Camera buffer size
#define BUFFER_PIC_NB 25

//Field waypoint tab length
#define TAB_LENGTH 28

// Etats de la mission
#define SCAN 0 //Scan the field (take pictures)
#define SENDING_PIC 1 //Sending the pictures
#define RTB 2 //Return above the base
#define RTW 3 //Return to work (last position before the drone came back to base)
#define CHARGE 4 //Charge at base
#define DISPLACEMENT_11 5 //Go to destination at 11m above the ground
#define DISPLACEMENT_5 6 //Go down to 5m
#define LANDING 7 //Land at base
#define FINISH 8 //The drone has finished its work

typedef struct
{
    double x;
    double y;
    double z;
} position;

typedef struct
{
   double orientation;
   double horizontal_speed;
   double vertical_speed;
} command;

double dist2D(position* p1, position* p2) {
    double x1 = p1->x;
    double x2 = p2->x;
    double y1 = p1->y;
    double y2 = p2->y;
    return sqrt(pow((x1-x2),2) + pow((y1-y2),2));
}

double dist3D(position* p1, position* p2){
    double x1 = p1->x;
    double x2 = p2->x;
    double y1 = p1->y;
    double y2 = p2->y;
    double z1 = p1->z;
    double z2 = p2->z;
    return sqrt(pow((x1-x2),2)+pow((y1-y2),2)+pow((z1-z2),2));
}

double distZ(position* p, position* target){
    return target->z-p->z;
}

void set_position(position* destination, position* target){
    destination->x = target->x;
    destination->y = target->y;
    destination->z = target->z;
}

// Data structure used for the task data
struct TaskData {
    position* destination; //Destination of the drone
    position* current_pos; //Current position of the drone
    command* current_command; //Current navigation command send to the hardware
    double last_orientation_command; //Last orientation command of the drone (used to calculate the current orientation command)
    double target_speed; //Targeted horizontal speed (1m/s when scanning / 10m/s ortherwise)
    position** waypoint_tab; //Waypoints to explore the field
    int waypoint_index; //Index in the waypoint tab of the current waypoint
    int mission_state; //State of the mission controler
    int last_mission_state; //State of the mission controlleur before an interruption (sending pictures)
    position** base_acces_positions; //Positions used to reach the base (0 : base / 1 : upward)
    position* return_position; //Position to reach after a low bat alarm an charge at base
    int buffer; //Picture buffer (number of pictures stored)
    int take_pic; //current picture taking command send to the hardware
    int send_pic; //current transmission command sens to the hardware
    bool need_resend; //Used to resent pictures if transmission failed (ex : low bat during transmission)
    int is_pic_taken; //input of the picture confirmation from the hardware
    bool last_pic_confirmed; //Used to be sure that the last picture was taken
    position* pos_last_picture; //Position of the drone when the last picture was taken
};


// Navigation Control task
double nav_ctrl_func(int segment, void* data) {
    TaskData *d = static_cast<TaskData*>(data);

    switch (segment) {
        case 1: //Read inputs

            d->current_pos->x = ttAnalogIn(POSX);
            d->current_pos->y = ttAnalogIn(POSY);
            d->current_pos->z = ttAnalogIn(POSZ);

            //mexPrintf("x : %lf / y : %lf / z : %lf \n",d->current_pos->x,d->current_pos->y,d->current_pos->z);

            return 0.003;

        case 2: //Compute command
            if (d->mission_state==SENDING_PIC){
                d->current_command->vertical_speed=0;
                if (dist2D(d->pos_last_picture,d->current_pos)>5.1){
                    d->current_command->orientation = atan2(d->destination->x-d->current_pos->x,d->destination->y-d->current_pos->y)*180/PI;
                    d->current_command->orientation = d->current_command->orientation+180;
                    d->current_command->horizontal_speed=0.1;
                    return 0.000006;
                }
                else{
                    d->current_command->horizontal_speed=0;
                    return 0.000004;
                }


                return 0.000001;
            }
            else{
                if (d->mission_state != CHARGE && d->mission_state!=FINISH){

                    d->current_command->orientation = atan2(d->destination->x-d->current_pos->x,d->destination->y-d->current_pos->y)*180/PI;

                    if (abs(d->current_command->orientation+360-d->last_orientation_command)<abs(d->current_command->orientation-d->last_orientation_command)){
                        d->current_command->orientation = d->current_command->orientation +360;
                    }

                    d->last_orientation_command = d->current_command->orientation;

                    if (dist2D(d->current_pos,d->destination)>0.1){
                        d->current_command->horizontal_speed = min(d->target_speed,dist2D(d->current_pos,d->destination));
                    }
                    else{
                        d->current_command->horizontal_speed = 0;
                    }

                    if (abs(distZ(d->current_pos, d->destination))>0.1){
                        if(distZ(d->current_pos, d->destination)>0){
                            d->current_command->vertical_speed = min(1.,0.5*distZ(d->current_pos, d->destination));
                        }
                        else{
                            d->current_command->vertical_speed = max(-1.,0.5*distZ(d->current_pos, d->destination));
                        }
                    }
                    else{
                        d->current_command->vertical_speed = 0;
                    }
                    return 0.000010;
                }

            }
            return 0.000001;


        case 3: //Check if the destination is reached
            if (d->mission_state != CHARGE && d->mission_state != SENDING_PIC && d->mission_state!=FINISH){
                //We arrived to the destination
                if (dist3D(d->current_pos,d->destination) < 0.1) {
                    mexPrintf("Arrived at destination\n");
                    //Create Job of the task that handle this information
                    ttCreateJob("arrived_to_dest");
                    return 0.000006;
                }
                return 0.000001;
            }
            return 0.000001;

        default: //Send command
            if (d->mission_state != CHARGE){
    //             Send outputs
                ttAnalogOut(CONS_OR, d->current_command->orientation);
                ttAnalogOut(CONS_HSPEED, d->current_command->horizontal_speed);
                ttAnalogOut(CONS_VSPEED, d->current_command->vertical_speed);
            }

            return FINISHED;
    }
}

//Reached destination handler
double arrived_to_dest(int segment, void* data) {
    TaskData *d = static_cast<TaskData*>(data);

    switch(segment) {
        case 1:
            switch (d->mission_state){
                case RTW:
                    set_position(d->destination,d->return_position);
                    d->destination->z=11;
                    d->mission_state = DISPLACEMENT_11;
                    d->target_speed = 10;
                    mexPrintf("DISPLACEMENT_11\n");
                    return 0.000007;

                case DISPLACEMENT_11:
                    d->destination->z=5;
                    d->mission_state = DISPLACEMENT_5;
                    mexPrintf("DISPLACEMENT_5\n");
                    return 0.000003;

                case DISPLACEMENT_5:
                //Relaunch the picture taking cycle
                    set_position(d->destination,d->waypoint_tab[d->waypoint_index]);
                    d->mission_state=SCAN;
                    d->target_speed = 1;
                    mexPrintf("SCAN\n");
                    return 0.000006;

                case SCAN:
                    d->waypoint_index++;
                    if (d->waypoint_index < TAB_LENGTH) {
                         //the Mission controler gives the new point to the Nav controler
                        set_position(d->destination,d->waypoint_tab[d->waypoint_index]);
                        mexPrintf("Next waypoint\n");
                        return 0.000006;
                    }
                    else {
                        //Retour à la base
                        mexPrintf("All points have been reached\n");
                        set_position(d->destination,d->base_acces_positions[1]);
                        d->mission_state = RTB;
                        d->target_speed = 10;
                        mexPrintf("RTB\n");
                        return 0.000009;
                    }
                    return 0.000001;

                case RTB:
                    set_position(d->destination,d->base_acces_positions[0]);
                    d->mission_state = LANDING;
                    mexPrintf("LANDING\n");
                    return 0.000005;

                case LANDING:

                    ttAnalogOut(CHARGE_BAT, 1);
                    mexPrintf("CHARGE\n");

                    if (d->waypoint_index < TAB_LENGTH) {
                        if(d->need_resend){
                            d->last_mission_state = CHARGE;
                            d->mission_state = SENDING_PIC;
                            d->send_pic = 1;
                            mexPrintf("Resend\n");
                        }
                        else{
                            d->mission_state = CHARGE;
                        }
                        return 0.000007;
                    }
                    else {
                      d->mission_state = FINISH;
                      //in case pictures are in the buffer at the end of the mission
                      if (d->buffer > 0) {
                        d->send_pic = 1;
                        mexPrintf("Sending last pictures remaining\n");
                        return 0.000004;
                      }
                      return 0.000002;
                    }

            }
        default:
            return FINISHED;
    }
}


//Low battery alarm handler
double batt_low_alarm(int segment, void* data){
    TaskData *d = static_cast<TaskData*>(data);

    switch (segment) {
        case 1:
            if (d->mission_state==SENDING_PIC){
                d->need_resend=true;
            }
            set_position(d->return_position,d->current_pos);
            set_position(d->destination,d->base_acces_positions[1]);
            d->target_speed = 10;
            d->mission_state = RTB;
            return 0.000010;
        default:
        mexPrintf("LOW_BAT\n");
            return FINISHED;
    }
}

//Full battery alarm handler
double batt_full_alarm(int segment, void* data){
    TaskData *d = static_cast<TaskData*>(data);

    switch (segment) {
        case 1:

            ttAnalogOut(CHARGE_BAT,0);

            if (d->mission_state == CHARGE){

                set_position(d->destination,d->base_acces_positions[1]);
                d->mission_state = RTW;

                return 0.000005;
            }
            return 0.000001;

        default:
            mexPrintf("FULL_BAT\n");
            return FINISHED;
    }
}

//Camera control task
double cam_ctrl_func(int segment, void* data) {
    TaskData *d = static_cast<TaskData*>(data);

    switch(segment) {
        case 1:
            //reading inputs
            d->is_pic_taken = ttAnalogIn(IS_PIC_TAKEN);
            return 0.001;

        case 2: //Check that last picture is taken and if the buffer is full
            if (!d->last_pic_confirmed){
                if(d->is_pic_taken==1){
                    d->buffer++;
                    d->last_pic_confirmed=true;
                    if (d->buffer == BUFFER_PIC_NB){
                        d->last_mission_state = d->mission_state;
                        d->mission_state=SENDING_PIC;
                        mexPrintf("SENDING_PIC\n");
                        d->send_pic = 1;
                        return 0.000009;
                    }
                }
            }
            return 0.000001;

        case 3: //Take pictures every 5m
            if(d->mission_state == SCAN){
                if(d->take_pic==1){
                    d->take_pic=0;
                }
                if (dist2D(d->pos_last_picture,d->current_pos)>4.9){
                    mexPrintf("Picture distance : %lf\n",dist2D(d->pos_last_picture,d->current_pos));
                    if (!d->last_pic_confirmed){
                        mexPrintf("Error : picture not taken\n");
                    }
                    d->take_pic =1;
                    set_position(d->pos_last_picture,d->current_pos);
                    d->last_pic_confirmed=false;
                    return 0.000012;
                }
            }
            return 0.000001;

        default:
          //send outputs
          ttAnalogOut(TAKE_PIC, d->take_pic);
          ttAnalogOut(SEND_PIC, d->send_pic);
          return FINISHED;
    }

}

//Transmission confirmed handler
double pic_sent(int segment, void* data) {
    TaskData *d = static_cast<TaskData*>(data);

    switch(segment) {
      case 1:
        d->send_pic = 0;
        if(d->mission_state==SENDING_PIC){
            d->buffer = 0;
            d->mission_state=d->last_mission_state;
            mexPrintf("PICS SENT\n");
            return 0.000005;
        }
        if(d->mission_state==FINISH) {
          d->buffer = 0;
          mexPrintf("Last pics sent\n");
          return 0.000004;
        }
        return 0.000001;

      default:
        return FINISHED;

    }
}



// Kernel init function
void init(){
    // Init task data
    TaskData *data = new TaskData;

    ttSetUserData(data);

    memset( data, 0, sizeof(TaskData) );

    //Init Camera buffer
    data->buffer = 0;

    //Init picture variables
    data->last_pic_confirmed = true;
    data->need_resend = false;
    position* pos_last_picture = new position;
    data->pos_last_picture=pos_last_picture;

    //Init command
    command* current_command = new command;
    data->current_command = current_command;
    data->last_orientation_command =0;

    //Init speed
    data->target_speed = 1;

    //Init state
    data->mission_state=RTW;

    //Start position
    position* current_pos = new position;
    current_pos->x=0;
    current_pos->y=0;
    current_pos->z=0;
    data->current_pos=current_pos;

    //Base acces
    position** base_acces_positions = new position*[2];
    position* base = new position;
    position* upward = new position;
    base->x = 0;
    base->y = 0;
    base->z = 0;
    upward->x = 0;
    upward->y = 0;
    upward->z = 11;
    base_acces_positions[0]=base;
    base_acces_positions[1]=upward;
    data->base_acces_positions=base_acces_positions;

    //Init destination
    position* destination = new position;
    set_position(destination,upward);
    data->destination=destination;

    // Field Waypoints
    data->waypoint_index = 0;
    position** waypoints = new position*[TAB_LENGTH];

    position* pos1 = new position;
    pos1->x = 20.5;
    pos1->y = 77.5;
    pos1->z = 5;

    position* pos2 = new position;
    pos2->x = 43.5;
    pos2->y = 77.5;
    pos2->z = 5;

    position* pos3 = new position;
    pos3->x = 43.5;
    pos3->y = 72.5;
    pos3->z = 5;

    position* pos4 = new position;
    pos4->x = 20.5;
    pos4->y = 72.5;
    pos4->z = 5;

    position* pos5 = new position;
    pos5->x = 20.5;
    pos5->y = 67.5;
    pos5->z = 5;

    position* pos6 = new position;
    pos6->x = 43.5;
    pos6->y = 67.5;
    pos6->z = 5;

    position* pos7 = new position;
    pos7->x = 43.5;
    pos7->y = 62.5;
    pos7->z = 5;

    position* pos8 = new position;
    pos8->x = 20.5;
    pos8->y = 62.5;
    pos8->z = 5;

    position* pos9 = new position;
    pos9->x = 20.5;
    pos9->y = 57.5;
    pos9->z = 5;

    position* pos10 = new position;
    pos10->x = 43.5;
    pos10->y = 57.5;
    pos10->z = 5;

    position* pos11 = new position;
    pos11->x = 43.5;
    pos11->y = 52.5;
    pos11->z = 5;

    position* pos12 = new position;
    pos12->x = 20.5;
    pos12->y = 52.5;
    pos12->z = 5;

    position* pos13 = new position;
    pos13->x = 12.5;
    pos13->y = 47.5;
    pos13->z = 5;

    position* pos14 = new position;
    pos14->x = 57.5;
    pos14->y = 47.5;
    pos14->z = 5;

    position* pos15 = new position;
    pos15->x = 57.5;
    pos15->y = 42.5;
    pos15->z = 5;

    position* pos16 = new position;
    pos16->x = 12.5;
    pos16->y = 42.5;
    pos16->z = 5;

    position* pos17 = new position;
    pos17->x = 12.5;
    pos17->y = 37.5;
    pos17->z = 5;

    position* pos18 = new position;
    pos18->x = 57.5;
    pos18->y = 37.5;
    pos18->z = 5;

    position* pos19 = new position;
    pos19->x = 57.5;
    pos19->y = 32.5;
    pos19->z = 5;

    position* pos20 = new position;
    pos20->x = 12.5;
    pos20->y = 32.5;
    pos20->z = 5;

    position* pos21 = new position;
    pos21->x = 12.5;
    pos21->y = 27.5;
    pos21->z = 5;

    position* pos22 = new position;
    pos22->x = 57.5;
    pos22->y = 27.5;
    pos22->z = 5;

    position* pos23 = new position;
    pos23->x = 57.5;
    pos23->y = 22.5;
    pos23->z = 5;

    position* pos24 = new position;
    pos24->x = 12.5;
    pos24->y = 22.5;
    pos24->z = 5;

    position* pos25 = new position;
    pos25->x = 12.5;
    pos25->y = 17.5;
    pos25->z = 5;

    position* pos26 = new position;
    pos26->x = 57.5;
    pos26->y = 17.5;
    pos26->z = 5;

    position* pos27 = new position;
    pos27->x = 57.5;
    pos27->y = 12.5;
    pos27->z = 5;

    position* pos28 = new position;
    pos28->x = 12.5;
    pos28->y = 12.5;
    pos28->z = 5;

    waypoints[0]=pos1;
    waypoints[1]=pos2;
    waypoints[2]=pos3;
    waypoints[3]=pos4;
    waypoints[4]=pos5;
    waypoints[5]=pos6;
    waypoints[6]=pos7;
    waypoints[7]=pos8;
    waypoints[8]=pos9;
    waypoints[9]=pos10;
    waypoints[10]=pos11;
    waypoints[11]=pos12;
    waypoints[12]=pos13;
    waypoints[13]=pos14;
    waypoints[14]=pos15;
    waypoints[15]=pos16;
    waypoints[16]=pos17;
    waypoints[17]=pos18;
    waypoints[18]=pos19;
    waypoints[19]=pos20;
    waypoints[20]=pos21;
    waypoints[21]=pos22;
    waypoints[22]=pos23;
    waypoints[23]=pos24;
    waypoints[24]=pos25;
    waypoints[25]=pos26;
    waypoints[26]=pos27;
    waypoints[27]=pos28;

    data->waypoint_tab=waypoints;

    //Init return position
    position* return_position = new position;
    set_position(return_position,pos1);
    data->return_position = return_position;

    ttInitKernel(prioFP);

    mexPrintf("Simulation started\n");

    ttCreatePeriodicTask("cam_ctrl_func",   0.0, CAM_CTRL_PERIOD,
                        cam_ctrl_func, data);
    ttSetPriority(3, "cam_ctrl_func");


    ttCreatePeriodicTask("nav_ctrl_func",   0.0, NAV_CTRL_PERIOD,
                        nav_ctrl_func, data);
    ttSetPriority(2, "nav_ctrl_func");


    ttCreateTask("arrived_to_dest",0.08,arrived_to_dest,data);
    ttSetPriority(2, "arrived_to_dest");

    ttCreateHandler("batt_low_alarm", 1, batt_low_alarm, data);
    ttAttachTriggerHandler(ALARM_BAT_10, "batt_low_alarm");

    ttCreateHandler("batt_full_alarm", 4, batt_full_alarm, data);
    ttAttachTriggerHandler(ALARM_BAT_FULL, "batt_full_alarm");

    ttCreateHandler("pic_sent", 3, pic_sent, data);
    ttAttachTriggerHandler(PIC_SENT, "pic_sent");




}

// Kernel cleanup function
void cleanup() {
    // Free the allocated memory
    delete (TaskData*) ttGetUserData();
}
