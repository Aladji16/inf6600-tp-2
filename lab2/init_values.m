%Variables initiales (modifiables)
INIT_NIV_BAT = 100;
TIME_TO_CHARGE_M = 1;
VITESSE_MAX_MS = 5;
BAT_AUTONOMY_M = 500;

TIME_PHOTO_S = 4;
TIME_TRANSM_S = 10;
PHOTO_FAIL_RATE = 1;


%Variables calcul�es � partir des pr�c�dentes (non modifiables)
TIME_TO_CHARGE_S = TIME_TO_CHARGE_M*60;