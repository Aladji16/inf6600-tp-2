%Variables initiales (modifiables)
%Niveau en %
INIT_NIV_RESERVOIR = 50;
%Taille en Litres
TAILLE_RESERVOIR = 10000;
%Debit en litres/secondes
DEBIT_REMPLISSAGE = 10;
DEBIT_VANNE = 15;

%Limites
HIGH_LVL_THRESHOLD = 90;
LOW_LVL_THRESHOLD = 10;
