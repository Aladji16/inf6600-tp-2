#define S_FUNCTION_NAME ctrlTank
#include "ttkernel.cpp"
#include <mex.h>

// System Outputs
#define VALVE_CTRL    1

// System Inputs
#define TANK_LEVEL       1

// System tasks periods (seconds)
#define CTRL_TASK_PERIOD    (60.0)
#define DISP_TASK_PERIOD    (10*60.0)

// System constants
#define HIGH_LVL_THRESHOLD  90
#define LOW_LVL_THRESHOLD   10


// Data structure used for the task data
struct TaskData {
  double tank_lvl;
  double open_valve;
};

double ctrl_function(int segment, void* data) {
    TaskData *d = static_cast<TaskData*>(data);
    
    switch (segment) {
        case 1:
            // read inputs from operative system
            d->tank_lvl = ttAnalogIn(TANK_LEVEL);
            return 5;
        case 2:
            if(d->tank_lvl <= LOW_LVL_THRESHOLD && d->open_valve)
            {
                mexPrintf("Closing Valve\n");
                d->open_valve = 0.0;
            }
            else if(d->tank_lvl >= HIGH_LVL_THRESHOLD && !d->open_valve)
            {
                mexPrintf("Opening Valve\n");
                d->open_valve = 1.0;
            }
            return 1;
        default:
            // send ouputs to operative system
            ttAnalogOut(VALVE_CTRL, d->open_valve);
            return FINISHED;
    }
}

double disp_function(int segment, void* data) {
    TaskData *d = static_cast<TaskData*>(data);
    
    switch (segment) {
        case 1:
            // read inputs from operative system
            d->tank_lvl = ttAnalogIn(TANK_LEVEL);
            return 5;
        case 2:
            // display tank level (using mexPrintf)
            mexPrintf("Current lvl: %f\n", d->tank_lvl);
            return 0.5;
        default:
            // last segment
            return FINISHED;
    }
}

// Kernel init function
void init(){
    // Allocate memory for the task and store pointer in UserData
    TaskData *data = new TaskData;
    ttSetUserData(data);
    memset( data, 0, sizeof(TaskData) );

    ttInitKernel(prioFP);

    mexPrintf("Simulation started\n");

    ttCreatePeriodicTask("task_ctrl", 0.0, CTRL_TASK_PERIOD,
                        ctrl_function, data);
    ttSetPriority(1, "task_ctrl");

    ttCreatePeriodicTask("task_disp",   0.0, DISP_TASK_PERIOD,
                        disp_function, data);
    ttSetPriority(2, "task_disp");
}

// Kernel cleanup function
void cleanup() {
    // Free the allocated memory
    TaskData *d = static_cast<TaskData*>(ttGetUserData());
    delete d;

    mexPrintf("Simulation ended\n");
}

