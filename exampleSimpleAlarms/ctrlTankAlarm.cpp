#define S_FUNCTION_NAME ctrlTankAlarm
#include "ttkernel.cpp"
#include <mex.h>

// System Outputs
#define VALVE_CTRL  1

// System Inputs
#define TANK_LEVEL  1

//System Triggers
#define TRIGGER_ALARM_LOW  1
#define TRIGGER_ALARM_HI   2

// System tasks periods (seconds)
#define CTRL_TASK_PERIOD    (60.0)
#define DISP_TASK_PERIOD    (10*60.0)


// Data structure used for the task data
struct TaskData {
  double tank_lvl;
  double open_valve;
};

double alarm_low(int segment, void* data) {
    TaskData *d = static_cast<TaskData*>(data);
    
    switch (segment) {
        case 1:
            mexPrintf("Closing Valve\n");
            d->open_valve = 0.0;
            return 1;
        default:
            // send ouputs to operative system
            ttAnalogOut(VALVE_CTRL, d->open_valve);
            return FINISHED;
    }
}

double alarm_hi(int segment, void* data) {
    TaskData *d = static_cast<TaskData*>(data);
    switch (segment) {
        case 1:
            mexPrintf("Opening Valve\n");
                d->open_valve = 1.0;
            return 1;
        default:
            // send ouputs to operative system
            ttAnalogOut(VALVE_CTRL, d->open_valve);
            return FINISHED;
    }
}

double disp_function(int segment, void* data) {
    TaskData *d = static_cast<TaskData*>(data);
    
    switch (segment) {
        case 1:
            // read inputs from operative system
            d->tank_lvl = ttAnalogIn(TANK_LEVEL);
            return 5;
        case 2:
            // display tank level (using mexPrintf)
            mexPrintf("Current lvl: %f\n", d->tank_lvl);
            return 0.5;
        default:
            // last segment
            return FINISHED;
    }
}

// Kernel init function
void init(){
    // Allocate memory for the task and store pointer in UserData
    TaskData *data = new TaskData;
    ttSetUserData(data);
    memset( data, 0, sizeof(TaskData) );

    ttInitKernel(prioFP);

    mexPrintf("Simulation started\n");

    ttCreatePeriodicTask("task_disp",   0.0, DISP_TASK_PERIOD,
                        disp_function, data);
    ttSetPriority(2, "task_disp");
    
    
    ttCreateHandler("alarm_low", 1, alarm_low, data);
    ttCreateHandler("alarm_hi", 1, alarm_hi, data);
    
    ttAttachTriggerHandler(TRIGGER_ALARM_LOW, "alarm_low");
    ttAttachTriggerHandler(TRIGGER_ALARM_HI, "alarm_hi");
}

// Kernel cleanup function
void cleanup() {
    // Free the allocated memory
    TaskData *d = static_cast<TaskData*>(ttGetUserData());
    delete d;

    mexPrintf("Simulation ended\n");
}

